/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// i18n
const i18n = () => {
  const options = document.getElementById('options') as HTMLHeadElement;
  options.textContent = browser.i18n.getMessage('options');

  const tabs = document.getElementById('tabs') as HTMLHeadElement;
  tabs.textContent = browser.i18n.getMessage('tabs');

  const backgroundTab = document.getElementById('background-tab') as HTMLLabelElement;
  backgroundTab.textContent = browser.i18n.getMessage('background');

  const clicks = document.getElementById('clicks') as HTMLHeadElement;
  clicks.textContent = browser.i18n.getMessage('clicks');

  const double = document.getElementById('double') as HTMLLabelElement;
  double.textContent = browser.i18n.getMessage('double');

  const tripple = document.getElementById('tripple') as HTMLLabelElement;
  tripple.textContent = browser.i18n.getMessage('tripple');
};

i18n();

const inputCheckbox = document.getElementById('checkbox-background') as HTMLInputElement;
const radioDouble = document.getElementById('radio-double') as HTMLInputElement;
const radioTripple = document.getElementById('radio-tripple') as HTMLInputElement;

const restore = async (): Promise<void> => {
  const storage = await browser.storage.local.get();
  inputCheckbox.checked = storage.background ? true : false;

  if (!storage.click || storage.click === 'double') {
    radioDouble.checked = true;
    return;
  }

  if (storage.click === 'tripple') {
    radioTripple.checked = true;
    return;
  }
};

document.addEventListener('DOMContentLoaded', restore);

const storeBackground = (): void => {
  browser.storage.local.set({
    background: inputCheckbox.checked,
  });
};

inputCheckbox.addEventListener('click', storeBackground);

const storeDoubleClick = () => {
  browser.storage.local.set({
    click: 'double',
  });
};

radioDouble.addEventListener('click', storeDoubleClick);

const storeTrippleClick = () => {
  browser.storage.local.set({
    click: 'tripple',
  });
};

radioTripple.addEventListener('click', storeTrippleClick);
