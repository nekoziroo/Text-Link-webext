/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

export interface Point {
  x: number;
  y: number;
}

export interface UrlSchemeRange {
  url: string;
  index: number;
  indexEnd: number;
  scheme: string;
}

export interface UrlRange {
  textIndex: number;
  index: number;
  indexEnd: number;
}

export interface Message {
  text: string;
  url: string;
  scheme: 'http' | 'domain';
  textArea: string;
  selectionEnd: number;
}

export interface MessageSelection {
  textArea: string;
  selectionEnd: number;
}
