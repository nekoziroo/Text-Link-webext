/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import MousePoint from './MousePoint';
import Text from './Text';
import TextArea from './TextArea';
import TextNodeCollector from './TextNodeCollector';

class DoubleClickHandler {
  public handleEvent(event: MouseEvent): void {
    if ((event.target as HTMLSelectElement).type) {
      const textArea = new TextArea(event);
      textArea.sendMessage();
    }
    const mousePoint = new MousePoint(event.x, event.y);
    const textNodeCollector = new TextNodeCollector((event.target as HTMLElement), mousePoint);
    if (!textNodeCollector.hasText()) {
      return;
    }
    const textNode = textNodeCollector.getTextNode();
    const text = new Text(textNode, mousePoint);
    text.openURL();
  }
}

export default DoubleClickHandler;
