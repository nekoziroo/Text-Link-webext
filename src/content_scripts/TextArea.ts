/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

class TextArea {
  private target: HTMLSelectElement;

  public constructor(event: MouseEvent) {
    this.target = event.target as HTMLSelectElement;
  }

  public sendMessage(): void {
    browser.runtime.sendMessage({
      textArea: this.target.value,
      selectionEnd: this.target.selectionEnd,
    });
  }
}
export default TextArea;
