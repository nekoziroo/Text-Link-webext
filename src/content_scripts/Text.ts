/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as Interface from '../interfaces/Interface';
import TextNodeRangeExtractor from './TextNodeRangeExtractor';
import TextNodeRectangle from './TextNodeRectangle';

class Text {
  private textNodes: Node[];
  private point: Interface.Point;

  public constructor(textNodes: Node[], point: Interface.Point) {
    this.textNodes = textNodes;
    this.point = point;
  }

  public async openURL(): Promise<void> {
    const urlIndex = await this.getMessage();
    if (!urlIndex) {
      return;
    }
    urlIndex.some(element => this.searchURL(element));
  }

  private async getMessage(): Promise<Interface.UrlSchemeRange[]> {
    const text = this.getTextAll();
    const urlIndex = await browser.runtime.sendMessage({
      text,
    }) as Interface.UrlSchemeRange[];

    return urlIndex;
  }

  private getTextAll(): string {
    const textContent = this.textNodes.map(textNode => textNode.textContent as string);
    const text = textContent.reduce((previousText, currentText) => previousText + currentText);
    return text;
  }

  private searchURL(element: Interface.UrlSchemeRange): boolean {
    const { index, indexEnd } = element;
    const urlRangeExtractor = new TextNodeRangeExtractor({
      index,
      indexEnd,
      textNodes: this.textNodes,
    });
    const indexArray = urlRangeExtractor.create();
    const result = indexArray.some(this.searchReact.bind(this));
    if (result) {
      this.sendURLMessage(element.url, element.scheme);
      return true;
    }
    return false;
  }

  private searchReact(element: Interface.UrlRange): boolean {
    const { textIndex, index, indexEnd } = element;
    const endOffset = this.endOffset(textIndex, indexEnd);
    const rect = new TextNodeRectangle({
      endOffset,
      node: this.textNodes[textIndex],
      startOffset: index,
    });
    return rect.isPointInRange(this.point);
  }

  private endOffset(textIndex: number, indexEnd: number): number {
    let endOffset: number;
    const textContentLength = (this.textNodes[textIndex].textContent as string).length;

    if (textContentLength < indexEnd) {
      endOffset = textContentLength;
    } else {
      endOffset = indexEnd;
    }

    return endOffset;
  }

  private sendURLMessage(url: string, scheme: string): void {
    browser.runtime.sendMessage({
      url,
      scheme,
    });
  }
}

export default Text;
