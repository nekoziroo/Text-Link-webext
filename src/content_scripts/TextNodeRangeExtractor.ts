/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as Interface from '../interfaces/Interface';

interface TextNodesRange {
  textNodes: Node[];
  index: number;
  indexEnd: number;
}

class TextNodeRangeExtractor {
  private readonly textNodes: Node[];
  private index: number;
  private indexEnd: number;
  private result: Interface.UrlRange[];
  private isFoundUrl: boolean;

  public constructor({ textNodes, index, indexEnd }: TextNodesRange) {
    this.textNodes = textNodes;
    this.index = index;
    this.indexEnd = indexEnd;
    this.result = [];
    this.isFoundUrl = false;
  }

  public create(): Interface.UrlRange[] {
    this.textNodes.some((textNodes, i) => this.findUrl(textNodes, i));
    return this.result;
  }

  private findUrl(textNode: Node, i: number): boolean {
    const textLength = (textNode.textContent as string).length;
    if (this.overRange()) {
      return true;
    }
    if (this.notFoundUrl(textLength)) {
      this.index -= textLength;
      this.indexEnd -= textLength;
      return false;
    }
    if (this.foundUrl()) {
      this.putUrlRangeIntoResult(0, textLength, i);
      return false;
    }
    this.putUrlRangeIntoResult(this.index, textLength, i);
    return false;
  }

  private overRange(): boolean {
    return this.indexEnd < 0;
  }

  private foundUrl(): boolean {
    return this.index > 0 && this.isFoundUrl;
  }

  private notFoundUrl(textLength: number): boolean {
    return textLength < this.index && !this.isFoundUrl;
  }

  private putUrlRangeIntoResult(index: number, textLength: number, i: number): void {
    const urlRange = {
      index,
      indexEnd: this.indexEnd,
      textIndex: i,
    };

    this.result.push(urlRange);
    this.isFoundUrl = true;
    this.indexEnd -= textLength;
  }
}

export default TextNodeRangeExtractor;
