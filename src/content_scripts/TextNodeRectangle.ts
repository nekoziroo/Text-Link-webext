import * as Interface from '../interfaces/Interface';

interface TextNodeRange {
  node: Node;
  startOffset: number;
  endOffset: number;
}

class TextNodeRectangle {
  private readonly node: Node;
  private readonly startOffset: number;
  private readonly endOffset: number;
  private rectangles!: ClientRectList | DOMRectList;
  private result!: boolean;

  public constructor({ node, startOffset, endOffset }: TextNodeRange) {
    this.node = node;
    this.startOffset = startOffset;
    this.endOffset = endOffset;
  }

  public isPointInRange(point: Interface.Point): boolean {
    this.createArea();
    this.someRectangles(point);
    return this.result;
  }

  private createArea(): void {
    const range = document.createRange();
    range.setStart(this.node, this.startOffset);
    range.setEnd(this.node, this.endOffset);
    this.rectangles = range.getClientRects();
  }

  private someRectangles(point: Interface.Point): void {
    this.result = Array.from(this.rectangles).some(
      rect =>
        rect.left <= point.x &&
        point.x <= rect.right &&
        rect.top <= point.y &&
        point.y <= rect.bottom,
    );
  }
}

export default TextNodeRectangle;
