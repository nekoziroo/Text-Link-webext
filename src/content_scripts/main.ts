/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import DoubleClickHandler from './DoubleClickHandler';

async function main(): Promise<void> {
  const storage = await browser.storage.local.get('click');
  const doubleClickHandler = new DoubleClickHandler();

  if (!storage.click || storage.click === 'double') {
    document.addEventListener('dblclick', doubleClickHandler);
    return;
  }

  if (storage.click === 'tripple') {
    document.addEventListener('click', (event: MouseEvent) => {
      if (event.detail === 3) {
        doubleClickHandler.handleEvent(event);
      }
    });
    return;
  }
}

main();
