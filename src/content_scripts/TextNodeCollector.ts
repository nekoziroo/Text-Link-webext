/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as Interface from '../interfaces/Interface';
import TextNodeRectangle from './TextNodeRectangle';

class TextNodeCollector {
  private readonly target: HTMLElement;
  private readonly point: Interface.Point;

  public constructor(target: HTMLElement, point: Interface.Point) {
    this.target = target;
    this.point = point;
  }

  public hasText(): boolean {
    const textNodes = this.collectChildNodes();
    const result = textNodes.some((node) => {
      const rect = new TextNodeRectangle({
        node,
        startOffset: 0,
        endOffset: (node.textContent as string).length,
      });
      return rect.isPointInRange(this.point);
    });
    return result;
  }

  public getTextNode(): Node[] {
    // target has child node and sibling node. has not
    if ((this.hasChildNode() && this.hasChildEmptyTag()) || this.hasSiblingNode()) {
      // target has sibling node
      return this.collectChildNodes(this.target);
    }

    // target does not have child sibling node
    if (!this.hasSiblingNodeInnerText()) {
      return this.collectChildNodes(this.target);
    }

    return this.collectChildNodes(this.target.parentNode as HTMLElement);
  }

  private collectChildNodes(node: HTMLElement | Node = this.target): Node[] {
    const nodes: Node[] = [];
    const treeWalker = document.createTreeWalker(node, NodeFilter.SHOW_TEXT);
    while (treeWalker.nextNode()) {
      nodes.push(treeWalker.currentNode);
    }

    return nodes;
  }

  private hasChildNode(): boolean {
    return this.target.childNodes.length !== 1;
  }

  private hasChildEmptyTag(): boolean {
    return this.target.childElementCount === 0;
  }

  private hasSiblingNode(): boolean {
    const element = this.target.nextElementSibling && this.target.previousElementSibling;
    return element instanceof HTMLElement;
  }

  private hasSiblingNodeInnerText(): boolean {
    const node: HTMLElement[] = [];
    let hasSiblingNodeInnerText = true;
    let element = this.target.nextElementSibling as HTMLElement;
    while (element) {
      node.push(element);
      element = element.nextElementSibling as HTMLElement;
    }
    element = this.target.previousElementSibling as HTMLElement;
    while (element) {
      node.push(element);
      element = element.previousElementSibling as HTMLElement;
    }
    node.forEach((item) => {
      if (item.innerText) {
        return;
      }

      if (item.childElementCount === 0) {
        return;
      }

      hasSiblingNodeInnerText = false;
    });

    return hasSiblingNodeInnerText;
  }
}

export default TextNodeCollector;
