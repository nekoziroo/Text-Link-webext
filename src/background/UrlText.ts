/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as Interface from '../interfaces/Interface';
import CONFIG from './CONFIG';

class UrlText {
  private readonly text: string;

  public constructor(text: string) {
    this.text = text;
  }

  public getIndex(): Interface.UrlSchemeRange[] {
    const urls: Interface.UrlSchemeRange[] = [];
    let results;
    // tslint:disable-next-line:no-conditional-assignment
    while ((results = CONFIG.REGEXP_HTTP.exec(this.text))) {
      const url = {
        scheme: CONFIG.SCHEME_HTTP,
        url: results[0],
        index: results.index,
        indexEnd: results.index + results[0].length,
      };
      urls.push(url);
    }
    // tslint:disable-next-line:no-conditional-assignment
    while ((results = CONFIG.REGEXP_DOMAIN.exec(this.text))) {
      const topLevelDomain = results[1]
        .slice(results[1].lastIndexOf(CONFIG.DOMAIN_SEPARAOTR) + 1)
        .toLocaleUpperCase();
      if (!CONFIG.TOP_LEVEL_DOMAIN.has(topLevelDomain)) {
        continue;
      }
      const url = {
        scheme: CONFIG.SCHEME_DOMAIN,
        url: results[0],
        index: results.index,
        indexEnd: results.index + results[0].length,
      };
      urls.push(url);
    }

    return urls;
  }
}

export default UrlText;
