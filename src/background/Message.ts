/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as Interface from '../interfaces/Interface';
import CONFIG from './CONFIG';
import Tab from './Tab';
import UrlFormatter from './UrlFormatter';
import UrlText from './UrlText';

class Message {
  public constructor() {
    browser.runtime.onMessage.addListener(this.onMessage.bind(this));
  }

  private onMessage(message: Interface.Message, {}: {}, sendResponse: ({}: {}) => void): void {
    if (CONFIG.MSG_PROPERTY_TEXT in message) {
      const urltext = new UrlText(message.text);
      sendResponse(urltext.getIndex());

      return;
    }

    if (CONFIG.MSG_PROPERTY_TEXTAREA in message) {
      const url = UrlFormatter.selection(message);
      if (url) {
        Tab.create(url);
      }
    } else {
      const url = UrlFormatter.createURL(message);
      Tab.create(url);
    }
  }
}

export default Message;
