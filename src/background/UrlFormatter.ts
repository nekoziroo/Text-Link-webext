/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as Interface from '../interfaces/Interface';
import CONFIG from './CONFIG';

class UrlFormatter {
  public static createURL(message: Interface.Message): string {
    if (message.scheme === CONFIG.SCHEME_HTTP) {
      return UrlFormatter.http(message.url);
    }
    return `${CONFIG.URL_SCHEME_HTTP}${message.url}`;
  }

  public static selection(message: Interface.Message): string {
    let result;
    let match;
    // tslint:disable-next-line:no-conditional-assignment
    while ((match = CONFIG.REGEXP_HTTP.exec(message.textArea))) {
      const url = match[0];
      const { index } = match;
      if (UrlFormatter.match(url, index, message.selectionEnd)) {
        result = UrlFormatter.http(url);
        break;
      }
    }
    return result as string;
  }

  private static http(url: string): string {
    const result = url.split(CONFIG.SCHEME_SEPARATOR);
    if (result.length === 1) {
      return `${CONFIG.URL_SCHEME_HTTP}${result[0]}`;
    }
    if (/s$/.test(result[0])) {
      return `${CONFIG.URL_SCHEME_HTTP}${result[1]}`;
    }
    return `${CONFIG.URL_SCHEME_HTTP}${result[1]}`;
  }

  private static match(match: string, index: number, selectionEnd: number): boolean {
    if (index < selectionEnd && index + match.length >= selectionEnd) {
      return true;
    }
    return false;
  }
}

export default UrlFormatter;
