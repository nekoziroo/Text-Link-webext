import { src, dest, parallel } from 'gulp';
import cleanCSS from 'gulp-clean-css';
import htmlmin from 'gulp-htmlmin';
import jsonMinify from 'gulp-json-minify';

const css = () => src('src/**/*.css', { base: 'src/' }).pipe(dest('./dist'));

const html = () => src('src/**/*.html', { base: 'src/' }).pipe(dest('./dist'));

const json = () => src('src/**/*.json', { base: 'src/' }).pipe(dest('./dist'));

const svg = () => src('src/**/*.svg', { base: 'src/' }).pipe(dest('./dist'));

export const copy = parallel(css, html, json, svg);

const minifyCss = () =>
  src('src/**/*.css', { base: 'src/' })
    .pipe(cleanCSS())
    .pipe(dest('./dist'));

const minifyHtml = () =>
  src('src/**/*.html', { base: 'src/' })
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(dest('./dist'));

const minifyJson = () =>
  src('src/**/*.json', { base: 'src/' })
    .pipe(jsonMinify())
    .pipe(dest('./dist'));

export const minify = parallel(minifyCss, minifyHtml, minifyJson, svg);

export default minify;
